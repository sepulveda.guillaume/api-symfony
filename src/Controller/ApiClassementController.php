<?php

namespace App\Controller;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Entity\Scores;
use App\Repository\ScoresRepository;
use Doctrine\DBAL\Driver\IBMDB2\Result;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;

class ApiClassementController extends AbstractController
{
    #[Route('/api/classement', name: 'api_classement_index', methods:["GET"])]
    public function index(ScoresRepository $repo, SerializerInterface $serializer): Response
    {
        $classement = $repo->findClassement();
        
        $get = $serializer->serialize($classement, 'json');

        $response = new Response($get, 200, [
            "Content-type" => "application/json"
        ]);

        return $response;
    }

    #[Route('/api/classement', name: 'api_classement_store', methods:["POST"])]
    public function store(Request $request, SerializerInterface $serializer, EntityManagerInterface $em, ValidatorInterface $validator) 
    {
        $json = $request->getContent();

        try {
            $post = $serializer->deserialize($json, Scores::class, 'json');

            $errors = $validator->validate($post);

            if(count($errors) > 0) {
                return $this->json($errors, 400);
            }
    
            $em->persist($post);
            $em->flush();
            
            return $this->json($post, 201, []);
        } catch(NotEncodableValueException $e) {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ], 400);
        }
    }

    #[Route('/api/classement/{id}', name: 'api_classement_remove', methods:["DELETE"])]
    public function remove(ScoresRepository $repo, EntityManagerInterface $em, $id) 
    {
        $itemRemove = $repo->find($id);
        
        if ($itemRemove != null) {
            $em->remove($itemRemove);
            $em->flush();
                
            return $this->json([
                'status' => 200,
                'message' => 'Le score a bien supprimé.'
            ]);
        } else {
            
            return $this->json([
                'status' => 400,
                'message' => 'Le score n\'a pas été supprimé car l\'id demandé n\'est pas valide.'
            ]);
        }
    }
}
