<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ScoresRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Filter\SearchFilter;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ScoresRepository::class)
 */
#[ApiResource(
    security: 'is_granted("ROLE_USER")', 
    collectionOperations: [
        'get_classement' => [
            "method" => "GET",
            "path" => "scores/classement",
            "order" => ["scoreGame" => "DESC"],
            "pagination_items_per_page" => 10,
            'openapi_context' => [
                'security' => [['bearerAuth' => []]]
            ] 
        ],
        'get' => [
            'openapi_context' => [
                'security' => [['bearerAuth' => []]]
            ]
        ],
        'post' => [
            'openapi_context' => [
                'security' => [['bearerAuth' => []]]
            ]
        ]
    ],
)]

class Scores
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */

    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private $scoreGame;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $scorePseudo;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;


    public function __construct()
    {
        $this->setCreatedAt(new \DateTimeImmutable());
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScoreGame(): ?int
    {
        return $this->scoreGame;
    }

    public function setScoreGame(int $scoreGame): self
    {
        $this->scoreGame = $scoreGame;

        return $this;
    }

    public function getScorePseudo(): ?string
    {
        return $this->scorePseudo;
    }

    public function setScorePseudo(string $scorePseudo): self
    {
        $this->scorePseudo = $scorePseudo;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
